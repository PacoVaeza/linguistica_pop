<div id="footer-area" class="jumbotron">
	<div class="footer-bar container">           
		<div class="row">

			<div class="col-md-4">
				<aside id="text-18" class="widget widget_text">
					<h4 class="widget-title">Acerca de Lingüística Pop</h4>			
					<div class="textwidget">
						<p>Matthew Remski writes and presents on yoga and ayurveda in the shadows of capitalism and climate change. He is the author of eight books of poetry, fiction, and non-fiction including <em>Threads of Yoga: A Remix of Patanjali’s Sutras</em>.</p>
						<p><a href="https://www.facebook.com/matthewremskiauthor" target="_blank" rel="noopener" style="color:#f7b858;">Follow on Facebook »</a><br />
						<a href="https://twitter.com/matthewremski" target="_blank" rel="noopener" style="color:#f7b858;">Follow on Twitter »</a></p>
					</div>
				</aside>
			</div>

			<div class="col-md-4">
				<aside id="cookie_latest_posts-3" class="widget widget_cookie_latest_posts">
					<h4 class="widget-title">Lastest Articles</h4>			
						<ul>					
							<li>													
					            <div class="latest-posts-thumbnail">
                            		<img width="150" height="75" src="http://matthewremski.com/wordpress/wp-content/uploads/2017/10/tunnel-150x75.png" class="attachment-thumbnail size-thumbnail wp-post-image" alt="11 Year-Old Boys, Touching Women on the Subway" srcset="http://matthewremski.com/wordpress/wp-content/uploads/2017/10/tunnel-150x75.png 150w, http://matthewremski.com/wordpress/wp-content/uploads/2017/10/tunnel-300x150.png 300w, http://matthewremski.com/wordpress/wp-content/uploads/2017/10/tunnel-768x384.png 768w, http://matthewremski.com/wordpress/wp-content/uploads/2017/10/tunnel-1024x512.png 1024w, http://matthewremski.com/wordpress/wp-content/uploads/2017/10/tunnel-1080x540.png 1080w, http://matthewremski.com/wordpress/wp-content/uploads/2017/10/tunnel-640x320.png 640w, http://matthewremski.com/wordpress/wp-content/uploads/2017/10/tunnel.png 1200w" sizes="(max-width: 150px) 100vw, 150px" />                        
                            	</div>
                                <div class="latest-posts-details">
                        			<h6 class="latest-posts-title">
                        				<a href="http://matthewremski.com/wordpress/eleven-year-old-boys-touching-women-subway/" rel="bookmark">Eleven-Year-Old Boys, Touching Women on the Subway</a>
                        			</h6>                        
                        			<span class="posted-on">
                        				<a href="http://matthewremski.com/wordpress/eleven-year-old-boys-touching-women-subway/" rel="bookmark">
                        					<time class="entry-date published" datetime="2017-10-26T08:28:32+00:00">October 26, 2017</time>
                        					<time class="updated" datetime="2017-10-27T11:47:12+00:00">October 27, 2017</time>
                        				</a>
                        			</span>                    
                        		</div>			
							</li>					
							<li>													
					            <div class="latest-posts-thumbnail">
                           			<img width="150" height="75" src="http://matthewremski.com/wordpress/wp-content/uploads/2017/10/Yod-150x75.jpg" class="attachment-thumbnail size-thumbnail wp-post-image" alt="The Guru Actually Hates You, and You Actually Hate Him" srcset="http://matthewremski.com/wordpress/wp-content/uploads/2017/10/Yod-150x75.jpg 150w, http://matthewremski.com/wordpress/wp-content/uploads/2017/10/Yod-300x150.jpg 300w, http://matthewremski.com/wordpress/wp-content/uploads/2017/10/Yod-768x384.jpg 768w, http://matthewremski.com/wordpress/wp-content/uploads/2017/10/Yod-1024x512.jpg 1024w, http://matthewremski.com/wordpress/wp-content/uploads/2017/10/Yod-1080x540.jpg 1080w, http://matthewremski.com/wordpress/wp-content/uploads/2017/10/Yod-640x320.jpg 640w, http://matthewremski.com/wordpress/wp-content/uploads/2017/10/Yod.jpg 1200w" sizes="(max-width: 150px) 100vw, 150px" />                        
                           		</div>
                                <div class="latest-posts-details">
                        			<h6 class="latest-posts-title">
                        				<a href="http://matthewremski.com/wordpress/guru-may-actually-hate-may-actually-hate/" rel="bookmark">The Guru May Actually Hate You, and You May Actually Hate Him</a>
                        			</h6>                        
                        			<span class="posted-on">
                        				<a href="http://matthewremski.com/wordpress/guru-may-actually-hate-may-actually-hate/" rel="bookmark">
                        					<time class="entry-date published" datetime="2017-10-21T08:08:59+00:00">October 21, 2017</time>
                        					<time class="updated" datetime="2017-10-21T14:16:29+00:00">October 21, 2017</time>
                        				</a>
                        			</span>                    
                				</div>			
							</li>			
						
							<li>													
					            <div class="latest-posts-thumbnail">
                            		<img width="150" height="91" src="http://matthewremski.com/wordpress/wp-content/uploads/2017/10/Schreber-150x91.png" class="attachment-thumbnail size-thumbnail wp-post-image" alt="Minimization As a Patriarchal Reflex" srcset="http://matthewremski.com/wordpress/wp-content/uploads/2017/10/Schreber-150x91.png 150w, http://matthewremski.com/wordpress/wp-content/uploads/2017/10/Schreber-300x181.png 300w, http://matthewremski.com/wordpress/wp-content/uploads/2017/10/Schreber-768x464.png 768w, http://matthewremski.com/wordpress/wp-content/uploads/2017/10/Schreber.png 1024w, http://matthewremski.com/wordpress/wp-content/uploads/2017/10/Schreber-640x386.png 640w" sizes="(max-width: 150px) 100vw, 150px" />                        
                            	</div>
                                <div class="latest-posts-details">
                        			<h6 class="latest-posts-title">
                        				<a href="http://matthewremski.com/wordpress/minimization-patriarchal-reflex/" rel="bookmark">On Minimization as a Patriarchal Reflex</a>
                        			</h6>                        
                        			<span class="posted-on">
                        				<a href="http://matthewremski.com/wordpress/minimization-patriarchal-reflex/" rel="bookmark">
                        					<time class="entry-date published updated" datetime="2017-10-20T08:01:24+00:00">October 20, 2017</time>
                        				</a>
                        			</span>                    
                        		</div>			
							</li>			
							
						</ul>	
					</aside>
				</div>

				<div class="col-md-4">
					<aside id="mc4wp_form_widget-3" class="widget widget_mc4wp_form_widget">
						<h4 class="widget-title">Sign up for updates</h4>
							<script type="text/javascript">(function() {
								if (!window.mc4wp) {
									window.mc4wp = {
										listeners: [],
										forms    : {
											on: function (event, callback) {
												window.mc4wp.listeners.push({
													event   : event,
													callback: callback
												});
											}
										}
									}
								}
							})();
							</script><!-- MailChimp for WordPress v4.1.8 - https://wordpress.org/plugins/mailchimp-for-wp/ -->
							<form id="mc4wp-form-1" class="mc4wp-form mc4wp-form-6420" method="post" data-id="6420" data-name="Sign up form" >
								<div class="mc4wp-form-fields">
									<p>
										<label>Email address: </label>
										<input type="email" name="EMAIL" placeholder="Your email address" required />
									</p>
									<p>
  										<input value="Sign up" type="submit">
									</p>	
									<div style="display: none;">
										<input type="text" name="_mc4wp_honeypot" value="" tabindex="-1" autocomplete="off" />
									</div>
									<input type="hidden" name="_mc4wp_timestamp" value="1509841113" />
									<input type="hidden" name="_mc4wp_form_id" value="6420" />
									<input type="hidden" name="_mc4wp_form_element_id" value="mc4wp-form-1" />
								</div>
								<div class="mc4wp-response"></div>
							</form><!-- / MailChimp for WordPress Plugin -->
					</aside>
				</div>               	
			</div>
	    </div>
	</div>
    
    <footer class="container">
    	<p class="float-right"><a href="#">Back to top</a></p>
        <p>&copy; 2017 Company, Inc. &middot; <a href="#">Privacy</a> &middot; <a href="#">Terms</a></p>
    </footer>


