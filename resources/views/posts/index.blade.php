@extends('layouts.master')

@section('content')
	
	<div id="myCarousel" class="carousel slide" data-ride="carousel" style="margin-bottom: 4rem;">
        <ol class="carousel-indicators">
          <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
          <li data-target="#myCarousel" data-slide-to="1"></li>
          <li data-target="#myCarousel" data-slide-to="2"></li>
        </ol>
        <div class="carousel-inner">
          <div class="carousel-item active">
            <img style="width: 100%;" class="first-slide"  src="https://1.bp.blogspot.com/_rTgyepwnfrU/SImG0E23CGI/AAAAAAAAAxM/KnnWAjc7Pn0/s400/Yllecaradesnu2.jpg" alt="First slide">
            <div class="container">
              <div class="carousel-caption text-left">
                <h1>Título de artículo de ejemplo</h1>
                <p>Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec id elit non mi porta gravida at eget metus. Nullam id dolor id nibh ultricies vehicula ut id elit.</p>
                <p><a class="btn btn-lg btn-primary" href="#" role="button">Sign up today</a></p>
              </div>
            </div>
          </div>
          <div class="carousel-item">
            <img class="second-slide" style="width: 100%; height: 30%;" src="https://1.bp.blogspot.com/_rTgyepwnfrU/SImG0E23CGI/AAAAAAAAAxM/KnnWAjc7Pn0/s400/Yllecaradesnu2.jpg" alt="Second slide">
            <div class="container">
              <div class="carousel-caption">
                <h1>Another Título de artículo de ejemplo</h1>
                <p>Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec id elit non mi porta gravida at eget metus. Nullam id dolor id nibh ultricies vehicula ut id elit.</p>
                <p><a class="btn btn-lg btn-primary" href="#" role="button">Learn more</a></p>
              </div>
            </div>
          </div>
          <div class="carousel-item">
            <img  class="third-slide" style="width: 100%; height: 30%;" src="https://1.bp.blogspot.com/_rTgyepwnfrU/SImG0E23CGI/AAAAAAAAAxM/KnnWAjc7Pn0/s400/Yllecaradesnu2.jpg" alt="Third slide">
            <div class="container">
              <div class="carousel-caption text-right">
                <h1>One more Título de artículo de ejemplo</h1>
                <p>Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec id elit non mi porta gravida at eget metus. Nullam id dolor id nibh ultricies vehicula ut id elit.</p>
                <p><a class="btn btn-lg btn-primary" href="#" role="button">Browse gallery</a></p>
              </div>
            </div>
          </div>
        </div>
        <a class="carousel-control-prev" href="#myCarousel" role="button" data-slide="prev">
          <span class="carousel-control-prev-icon" aria-hidden="true"></span>
          <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#myCarousel" role="button" data-slide="next">
          <span class="carousel-control-next-icon" aria-hidden="true"></span>
          <span class="sr-only">Next</span>
        </a>
     </div>


      <!-- Marketing messaging and featurettes
      ================================================== -->
      <!-- Wrap the rest of the page in another container to center all the content. -->
      <div class="container marketing">

        <!-- Three columns of text below the carousel -->
        <div class="row">
          @foreach($posts as $post)
            <div class="col-lg-4">
              <img class="rounded-circle" src="https://decagono.com/img/circulo.png" alt="Generic placeholder image" width="140" height="140">
              <h2>{{$post->title}}</h2>
              <p>{{$post->body}}</p>
              <p><a class="btn btn-secondary" href="#" role="button">View details &raquo;</a></p>
            </div><!-- /.col-lg-4 -->
          @endforeach
        </div><!-- /.row -->


        <!-- START THE FEATURETTES -->

        <hr class="featurette-divider">

        @foreach($posts as $post)
          @if((($post->id)%2 == 0))
          
              <div class="row featurette">
                <div class="col-md-7">
                  <h2 class="featurette-heading">{{$post->title}} <span class="text-muted">{{$post->subtitle}}</span></h2>
                  <p class="lead">Donec ullamcorper nulla non metus auctor fringilla. Vestibulum id ligula porta felis euismod semper. Praesent commodo cursus magna, vel scelerisque nisl consectetur. Fusce dapibus, tellus ac cursus commodo.</p>
                </div>
                <div class="col-md-5">
                  <img class="featurette-image img-fluid mx-auto" src="https://decagono.com/img/circulo.png" alt="Generic placeholder image">
                </div>
              </div>

              <hr class="featurette-divider">
            @else 
              <div class="row featurette">
                <div class="col-md-7 order-md-2">
                  <h2 class="featurette-heading">{{$post->title}} <span class="text-muted">{{$post->subtitle}}</span></h2>
                  <p class="lead">Donec ullamcorper nulla non metus auctor fringilla. Vestibulum id ligula porta felis euismod semper. Praesent commodo cursus magna, vel scelerisque nisl consectetur. Fusce dapibus, tellus ac cursus commodo.</p>
                </div>
                <div class="col-md-5 order-md-1">
                  <img class="featurette-image img-fluid mx-auto" src="https://decagono.com/img/circulo.png" alt="Generic placeholder image">
                </div>
              </div>

              <hr class="featurette-divider">
          
          @endif
        @endforeach

        <!-- /END THE FEATURETTES -->

      </div><!-- /.container -->
@endsection