@extends('layouts.master')

@section('content')
        <div class="row" style="margin-top: 4rem">
          <div class="col-lg-6">
	<div class="col-sm-8 blog-main">

		<h1>Here you'll create your own posts</h1>
		
		<hr>

		<form method="POST" action="/posts">
		   {{ csrf_field() }}

		  <div class="form-group">
		    <label for="title">Title</label>
		    <input type="text" class="form-control" id="title" name="title" required>
		  </div>

		  <div class="form-group">
		    <label for="subtitle">Subtitle</label>
		    <input type="text" class="form-control" id="subtitle" name="subtitle" required>
		  </div>

		  <div class="form-group">
		    <label for="body">Body</label>
		    <textarea class="form-control" id="body" name="body" required></textarea>
		  </div>

		  <div class="form-group">
		  	<button type="submit" class="btn btn-primary">Publish</button>
		  </div>

		  @include('layouts.errors')

		
		</form>
	</div>
</div>
</div>

@endsection